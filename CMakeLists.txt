cmake_minimum_required ( VERSION 3.11 )

project ( RTWeekendCuda
  VERSION 2.95.0
  )

# Set to c++11
set ( CMAKE_CXX_STANDARD 11 )

include(CheckLanguage)
check_language(CUDA)
if(CMAKE_CUDA_COMPILER)
  enable_language(CUDA)
  if(NOT DEFINED CMAKE_CUDA_STANDARD)
    set(CMAKE_CUDA_STANDARD 11)
    set(CMAKE_CUDA_STANDARD_REQUIRED ON)
  endif()
  set(CMAKE_CUDA_SEPARABLE_COMPILATION ON) # for multi-target pre-compilations
else()
  message(STATUS "No CUDA support")
endif()



set ( COMMON_ALL_CUDA
  commonCuda/rtweekend.cuh
  commonCuda/camera.cuh
  commonCuda/ray.cuh
  commonCuda/vec3.cuh
)

set ( SOURCE_ONE_WEEKEND_CUDA
  ${COMMON_ALL_CUDA}
  InOneWeekend/hittable.cuh
  InOneWeekend/hittable_list.cuh
  InOneWeekend/material.cuh
  InOneWeekend/sphere.cuh
  InOneWeekend/main.cu
)

add_executable(inOneWeekendCuda  ${SOURCE_ONE_WEEKEND_CUDA})

target_include_directories(inOneWeekendCuda  PRIVATE .)
